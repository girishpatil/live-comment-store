// Express

const express = require('express');
const path = require('path');
const app = express();

app.use(express.json());  
app.use(express.static(path.resolve(__dirname + '/statics')))

//  Mongo DB 

const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'hashnode_posts';

//  Memcached

var Memcached = require('memcached');
var memcached = new Memcached('localhost:11211');
const CACHE_TIME = 60 * 5;

// Custom response object

let response = {
    msg : '',
    code : 400, // 400 is error | 200 is success
    data : null
}

/**
 * Route : /
 * simply sends the template file
 */

app.get('/', (req , res) => {
    
    res.sendFile( path.resolve(__dirname+'/templates/comments.html'));
    
})

/**
 * Route : /flush
 * Just in case if needed to flush the memcache memory.
 */

app.get('/flush', (req, res) => {

    memcached.flush(function(err){

        res.send("Flushed")
    });

})

/**
 * saveComment() saves the comment stored in 
 * In-memory DB (memcached) into MongoDB.
 * 
 */

saveComment = ( post , comment ) => {

    return MongoClient.connect(url, function (err, client) {

        const db = client.db(dbName);

        db.collection('posts').update(
            {
                id: post.postId,
                "comments.commentId": post.commentId,
            },
            {
                id: post.postId,
                "comments": {
                    commentId: post.commentId,
                    commentAt: new Date,
                    commentText: comment
                }
            },
            {
                upsert: true
            }
        ).then((res) => {

            client.close();

            response.msg = "Comment saved successfully";
            response.code = 200;

            return ;

        }).catch((err) => {

            client.close();

            response.msg = "Error in commenting";
            response.code = 400;

            return ;

        })

    });
    
}

/**
 * Route /comments/store
 * the main endpoint that will be hit the most.
 */

app.post('/comments/store' , (req , res) => {

    let post = req.body;

    console.log(post)

    // If the comment has to be saved

    if (post.commentType == 'S') {

        memcached.get(post.commentId, function (err, data) {
            
            if (err) {
                
                response.msg = 'Error in storing comments';
                response.code = 400;
                
                res.send(response);
                
            }
            
            data = (data) ? data : '';
            
            let comment = (data + post.comment);
            
            console.log("--data", data)
            console.log("--comment",comment)
            
            // Save the data to main DB
            saveComment( post , comment );
            
            res.send(response);
            
        })

    }

    // If the comment has to be Appended to the old one
    
    else if(post.commentType == 'A'){

        //  Append this incoming text to the old one

        memcached.append(post.commentId, post.comment, function (err, data) {

            console.log("Append",post.comment)
            
            if (err) {
                response.msg = 'Error in storing comments';
                response.code = 400;

                res.send(response);
                return
            }

            response.msg = 'Appended';
            response.code = 200;

            res.send(response)

        })
            
        
    }
    
    // If the comment has to be store as new

    else if(post.commentType == 'N'){

        // Create a new comment and store the incoming details in Memcache.

        memcached.get(post.commentId , function(err, data ){
            
            /** If there is a comment in Memcached mem already one with the incoming ID then get it 
             * else create a new one
            */
            
            if(data){
                console.log("Found",data)
                memcached.replace(post.commentId, post.comment, CACHE_TIME, function (err, data) {

                    if (err) {
                        console.error(err);

                        response.msg = "Error in saving comment";
                        response.code = 400;
                        res.send(response)

                    }

                    response.msg = "Created";
                    response.code = 200;

                    memcached.end();
                    res.send(response)


                });


            }else{

                console.log("Creating", data)

                memcached.set(post.commentId, post.comment, CACHE_TIME, function (err, data) {

                    if (err) {
                        console.error(err);

                        response.msg = "Error in saving comment";
                        response.code = 400;
                        res.send(response)

                    }

                    response.msg = "Created";
                    response.code = 200;

                    memcached.end();
                    res.send(response)


                });
                
            }
            
        })
        
    }else{

        // Respont with normal error
        
        response.code == 400;
        response.msg = "Request error";
    
        res.send(response);

    }



})

app.listen(3000,() => {
    console.log( "Hashnode comment system running @ localhost:3000");
})

