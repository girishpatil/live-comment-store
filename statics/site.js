/**
 * anonymous function()
 * just to encapsulte the whole component
 * takes the comment INPUT and SUBMIT button as parameters
 */

var timer = null;
const POST_ID = 'posthash13'; // Just a hash for the current post ( this will be from server )
const COMMENT_ID = 'commenthash12'; // This can be generated on client and pushed or when user clicks on comment button a server request can do this.

window.onload = 
(function( commentElement , submitButton ){

    this.saveGap = 1000;
    this.incomingComment = ''; 
    this.savedComment = '';
    this.deltaComment = '';
    this.focusListener = null;
    this.upListener = null;
    this.downListener = null;
    this.commentType = 'N'; // A ( Append ) | N ( New ) | S ( Save )

    self = this;

    /**
     * getDelta() finds the difference between the 
     * last-pushed comment and current incoming comment.
     * - Finds if total change is needed or just new append
     */

    this.getDelta = function(){

        if (self.savedComment.length > 0) {
            
            
            if(self.commentType != 'S'){
                
                if (self.incomingComment.includes(self.savedComment)) {
                    console.log('A');
                    self.commentType = 'A'
                } else {
                    console.log(self.savedComment ,"||", self.incomingComment)
                    console.log('N');
                    self.commentType = 'N';
                }
                
            }
            
            var delta = self.incomingComment.split(self.savedComment);
            
            if( delta[1] ){
                return delta[1]; // for the very first time when the self.savedComent is ''
            }else{
                return delta[0]; // for every other time self.savedComment is !''
            }

        }
        else {

            return self.incomingComment;

        }


    }

    this.updateStatus = function(s){

        document.getElementById("status").textContent = s;
        
    }

    /**
     * save() does the web request to save|append|create|update the comment
     * 
     */

    this.save = function(){

        var delta = self.getDelta();

        if(self.savedComment != delta.trim()){

            if(self.commentType == 'A'){
                self.savedComment += delta;
            }else if(self.commentType == 'N'){
                self.savedComment = delta;
            }

            console.log("Saved", delta, new Date)

            axios.post('/comments/store', {
                commentType: self.commentType,
                comment: delta,
                commentId: COMMENT_ID,
                postId: POST_ID

            }).then((res) => {

                console.log(res)
                self.updateStatus('Saved')

            }).catch((err) => {

                console.log(err);

            });

            self.commentType = 'A';

            clearTimeout(timer);
            
        }
        
        
    }

    // commentFocused() when the comment input element is focused

    this.commentFocused = function(){

        self.updateStatus("Typing..")

        commentElement.addEventListener("keyup", function (e) {

           self.updateStatus("Typing..")
            
           if(e.keyCode != 32){ // if not space

               if (timer) {
                   // console.log("Cleared");
                   clearTimeout(timer);
               }

               self.incomingComment = e.target.value;

               timer = setTimeout(function () {

                   self.save();

               }, self.saveGap);
               
           }
            
        })
        
    }

    commentElement.addEventListener( "focus" , this.commentFocused );

    commentElement.addEventListener("blur",function(){

        // self.save();
        
        commentElement.removeEventListener("keyup", self.upListener);
        commentElement.removeEventListener("keydown",self.downListener);
        clearTimeout(timer)
        timer = null;
        
    })
    
    submitButton.addEventListener("click", function(){
        self.commentType = 'S'
        self.save()
    });

})(document.querySelector("#comment-area"), document.querySelector("#comment-submit"))
