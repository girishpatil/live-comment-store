/**
 * anonymous function()
 * just to encapsulte the whole component
 * takes the comment INPUT and SUBMIT button as parameters
 */

var timer = null;
const POST_ID = 'posthash13'; // Just a hash for the current post ( this will be from server )
const COMMENT_ID = 'commenthash12'; // This can be generated on client and pushed or when user clicks on comment button a server request can do this.

window.onload =
    (function (commentElement, submitButton) {

        this.saveGap = 1000;
        this.incomingComment = '';
        this.savedComment = '';
        this.deltaComment = '';
        this.focusListener = null;
        this.upListener = null;
        this.downListener = null;
        this.commentType = 'N'; //  N ( New ) | S ( Save )
        this.DMP = new diff_match_patch();
        self = this;

        this.updateStatus = function (s) {

            document.getElementById("status").textContent = s;

        }

        /**
         * save() does the web request to save|append|create|update the comment
         * 
         */

        this.save = function () {
            
            // console.log(self.savedComment,"@@@",self.incomingComment)
            var diff = self.DMP.diff_main(self.savedComment, self.incomingComment);

            var patch = self.DMP.patch_make(self.savedComment, diff)
            
            self.savedComment = self.DMP.patch_apply(patch, self.savedComment)[0];
            
            console.log("Save this", diff)
            console.log("Saved comment" , self.savedComment)

            axios.post('/comments/store', {
                commentType: self.commentType,
                comment: diff,
                commentId: COMMENT_ID,
                postId: POST_ID

            }).then((res) => {

                console.log(res)
                self.updateStatus('Saved')

            }).catch((err) => {

                console.log(err);

            });

            self.commentType = 'N';
            

            clearTimeout(timer);
            
        }

        // commentFocused() when the comment input element is focused

        this.commentFocused = function () {

            self.updateStatus("Typing..")

            commentElement.addEventListener("keyup", function (e) {

                self.updateStatus("Typing..")

                if (e.keyCode != 32) { // if not space

                    if (timer) {
                        // console.log("Cleared");
                        clearTimeout(timer);
                    }

                    self.incomingComment = e.target.value;

                    timer = setTimeout(function () {

                        self.save();

                    }, self.saveGap);

                }

            })

        }

        commentElement.addEventListener("focus", this.commentFocused);

        commentElement.addEventListener("blur", function () {

            // self.save();

            commentElement.removeEventListener("keyup", self.upListener);
            commentElement.removeEventListener("keydown", self.downListener);
            clearTimeout(timer)
            timer = null;

        })

        submitButton.addEventListener("click", function () {
            self.commentType = 'S'
            self.save()
        });

    })(document.querySelector("#comment-area"), document.querySelector("#comment-submit"))
