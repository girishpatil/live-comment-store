# Live comment system

#### Main dependencies.
Mongo DB [Ubuntu 16  download link](https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1604-4.0.0.tgz)
Memcached [Link](https://memcached.org/)
Nodejs

### Get started
In project dir,
  ``` npm install```
  Gotta keep Mongodb server 
  Turn on memcached service on
  ``` node app.js```

#### Plan 
  This can be done by using sockets or
  Normal HTTP request every now and then.

Ubuntu 16 , Node v9 , npm 5.6 , SCSS , HTML, Vanilla JS , Nodejs, MongoDB, Memcached.

#### Frontend 
Built on HTML,SCSS, Vanilla js. I can build a vuejs version of this too.

--
#### Backend
  The main strategy was keeping in mind a more medium scale app receiving lots of traffic.  
  Instead of hitting the main endpoint to store every now and then when the user stops.  
  I thought its better to place an in-memory db engine to speed things up and can do more number of read/writes.  
  here I have used memcached.  
  When user hits comment button this data from memcached is moved to the main Mongo DB.  
