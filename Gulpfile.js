const gulp = require('gulp');
const sass = require('gulp-sass');

gulp.task('styles',function(){

    gulp.src('./statics/site.scss')
    .pipe(sass().on('error',sass.logError))
    .pipe(gulp.dest('./statics/'))
    
})

gulp.task('default',function(){

    gulp.watch('./statics/site.scss',['styles'])
    
})