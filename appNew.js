// Express

const express = require('express');
const path = require('path');
const app = express();

const DF = require('diff-match-patch')
const dmp = new DF();

app.use(express.json());
app.use(express.static(path.resolve(__dirname + '/statics')))

//  Mongo DB 

const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'hashnode_posts';

//  Memcached

var Memcached = require('memcached');
var memcached = new Memcached('localhost:11211');
const CACHE_TIME = 60 * 5;

// Custom response object

let response = {
    msg: '',
    code: 400, // 400 is error | 200 is success
    data: null
}

/**
 * Route : /
 * simply sends the template file
 */

app.get('/', (req, res) => {

    res.sendFile(path.resolve(__dirname + '/templates/comments.html'));

})

/**
 * Route : /flush
 * Just in case if needed to flush the memcache memory.
 */

app.get('/flush', (req, res) => {

    memcached.flush(function (err) {
        console.log("Flushed")
        res.send("Flushed")
    });

})

/**
 * saveComment() saves the comment stored in 
 * In-memory DB (memcached) into MongoDB.
 * 
 */

saveComment = (post, comment) => {

    return MongoClient.connect(url, function (err, client) {

        const db = client.db(dbName);

        db.collection('posts').update(
            {
                id: post.postId,
                "comments.commentId": post.commentId,
            },
            {
                id: post.postId,
                "comments": {
                    commentId: post.commentId,
                    commentAt: new Date,
                    commentText: comment
                }
            },
            {
                upsert: true
            }
        ).then((res) => {

            client.close();

            response.msg = "Comment saved successfully";
            response.code = 200;

            return;

        }).catch((err) => {

            client.close();

            response.msg = "Error in commenting";
            response.code = 400;

            return;

        })

    });

}

/**
 * Route /comments/store
 * the main endpoint that will be hit the most.
 */

app.post('/comments/store', (req, res) => {

    let post = req.body;

    var commentData = post.comment; // this is the diff

    // If the comment has to be saved

    if (post.commentType == 'S') {

        memcached.get(post.commentId, function (err, data) {

            if (err) {

                response.msg = 'Error in storing comments';
                response.code = 400;

                res.send(response);

            }

            data = (data) ? data : '';

            const patch = dmp.patch_make(data, commentData)

            const apply = dmp.patch_apply(patch, data);

            // Save the data to main DB
            saveComment(post, apply[0]);

            console.log("Saved", apply[0])

            res.send(response);

        })

    }

    // If the comment has to be store as new

    else {

        // Create a new comment and store the incoming details in Memcache.

        memcached.get(post.commentId, function (err, data) {

            /** If there is a comment in Memcached mem already one with the incoming ID then get it 
             * else create a new one
            */

            var commentData  = post.comment; // this is the diff

            if (data) { // last saved comment

                // const diff = dmp.diff_main(data, commentData);
               
                const patch = dmp.patch_make(data, commentData)
    
                const apply = dmp.patch_apply(patch, data)[0];

                console.log("Present & replaced", apply[0])

                if(commentData.length == 0) apply  = '';
               
                memcached.replace(post.commentId, apply , CACHE_TIME, function (err, data) {
                    
                    if (err) {
                        console.error(err);

                        response.msg = "Error in saving comment";
                        response.code = 400;
                        res.send(response)

                    }

                    response.msg = "Created";
                    response.code = 200;

                    memcached.end();
                    res.send(response)

                });


            } else {

                console.log("###",commentData);

                // const diff = dmp.diff_main("", commentData);

                const patch = dmp.patch_make("", commentData)

                const apply = dmp.patch_apply(patch, "");

                memcached.set(post.commentId, apply[0], CACHE_TIME, function (err, data) {

                    console.log("Created" , apply[0]);
                    
                    if (err) {
                        console.error(err);

                        response.msg = "Error in saving comment";
                        response.code = 400;
                        res.send(response)

                    }

                    response.msg = "Created";
                    response.code = 200;

                    memcached.end();
                    res.send(response)

                });

            }

        })

    }

})

app.listen(3000, () => {
    console.log("Hashnode comment system running @ localhost:3000");
})
